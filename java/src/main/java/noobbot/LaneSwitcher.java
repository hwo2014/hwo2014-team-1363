/*
 * The MIT License
 *
 * Copyright 2014 Abdul Qadir, Murtaza Raza and Mustafa Hussain.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package noobbot;

import java.util.ArrayList;
import java.util.List;
import noobbot.Deserializer.*;

public class LaneSwitcher {
    public List<Integer> switchPieceIndexes;
    public List<Integer> switchLanePreference;
    
    public void calculateIdealSwitches(GameInitData gameData) {
        // SHORTEST DISTANCE METHOD
        // 1. Calculate distances of each piece per lane
        // 2. Find switch pieces
        // 3. Find the shortest lane between two consecutive pieces
        // 4. Find the shortest lane between the last switch and the finish line
        // 5. Restart calculation on lap finish
        
        switchPieceIndexes = new ArrayList<>();
        switchLanePreference = new ArrayList<>();
        double[][] laneDistances = new double[gameData.race.track.lanes.length][gameData.race.track.pieces.length];
        
        for(int i = 0; i < gameData.race.track.pieces.length; i++) {
            // 1. Calculate distances of each piece per lane
            for (int j = 0; j < gameData.race.track.lanes.length; j++) {
                if (gameData.race.track.pieces[i].length != 0) {
                    laneDistances[j][i] = gameData.race.track.pieces[i].length;
                } else if ((gameData.race.track.pieces[i].length == 0) && (gameData.race.track.pieces[i].angle > 0)){
                    laneDistances[j][i] = Math.abs((gameData.race.track.pieces[i].radius - gameData.race.track.lanes[j].distanceFromCenter) * Math.toRadians(gameData.race.track.pieces[i].angle));
                } else if ((gameData.race.track.pieces[i].length == 0) && (gameData.race.track.pieces[i].angle < 0)){
                    laneDistances[j][i] = Math.abs((gameData.race.track.pieces[i].radius + gameData.race.track.lanes[j].distanceFromCenter) * Math.toRadians(gameData.race.track.pieces[i].angle));
                }
            }
            /*System.out.println("Piece " + i + ": " + laneDistances[0][i] + ", " + laneDistances[1][i]);
            System.out.println();*/
            
            // 2. Find switch pieces
            if (gameData.race.track.pieces[i].laneSwitch == true) {
                switchPieceIndexes.add(i);
            }
        }
        
        switchPieceIndexes.add(gameData.race.track.pieces.length);
        // 3. Find the shortest lane between two consecutive pieces
        for(int i = 0; i < switchPieceIndexes.size() - 1; i++) {
            double[] inSwitchDistances = new double[gameData.race.track.lanes.length];
            for (int j = switchPieceIndexes.get(i) + 1; j < switchPieceIndexes.get(i + 1); j++) {
                for (int k = 0; k < gameData.race.track.lanes.length; k++) {
                    inSwitchDistances[k] += laneDistances[k][j];
                }
            }
            if (inSwitchDistances[0] < inSwitchDistances[1]) {
                switchLanePreference.add(0);
            } else {
                switchLanePreference.add(1);
            }
            /*System.out.println("Distances between switches " + switchPieceIndexes.get(i) + " & " + switchPieceIndexes.get(i + 1) + " is " + inSwitchDistances[0] + ", " + inSwitchDistances[1]);
            System.out.println("Lane " + switchLanePreference.get(i) + " prefered");*/
        }
        switchPieceIndexes.remove(switchPieceIndexes.size() - 1);
        
        //removeIdenticals();
    }
    
    /*private void removeIdenticals() {
        List<Integer> identicalIndexes = new ArrayList<>();
        
        int num = switchPieceIndexes.size();
        
        for (int i = num - 1; i > 0; i--) {
            if (switchLanePreference.get(i) == switchLanePreference.get(i - 1)) {
                switchLanePreference.remove(i);
                switchPieceIndexes.remove(i);
            }
        }
        
        System.out.println(switchLanePreference + " @ " + switchPieceIndexes);
    }*/
    
    public String switchAction(CarPositionData carPosition) {
        String returnString = null;
        if (switchLanePreference.get(0) != carPosition.piecePosition.lane.endLaneIndex) {
            if (carPosition.piecePosition.lane.endLaneIndex < switchLanePreference.get(0)) {
                returnString = "Right";
            } else if (carPosition.piecePosition.lane.endLaneIndex > switchLanePreference.get(0)) {
                returnString = "Left";
            }
        }
        switchPieceIndexes.remove(0);
        switchLanePreference.remove(0);
        return returnString;
    }
}
