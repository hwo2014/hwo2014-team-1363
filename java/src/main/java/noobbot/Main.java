/*
 * The MIT License
 *
 * Copyright 2014 Abdul Qadir, Murtaza Raza and Mustafa Hussain.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package noobbot;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;

import com.google.gson.Gson;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import noobbot.Deserializer.*;
import noobbot.Serializer.BotId;
import noobbot.Serializer.JoinRace;
import noobbot.Serializer.JoinRaceData;
import noobbot.Serializer.Ping;
import noobbot.Serializer.SwitchLanes;
import noobbot.Serializer.Throttle;

public class Main {
    public static void main(String... args) throws IOException {
        String host = args[0];
        int port = Integer.parseInt(args[1]);
        String botName = args[2];
        String botKey = args[3];
        
        System.out.println("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

        final Socket socket = new Socket(host, port);
        final PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));

        final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));

        new Main(reader, writer, new JoinRace(new JoinRaceData(new BotId(botName, botKey), "keimola", 1)));
    }

    final Gson gson = new Gson();
    
    public SpeedoMeter mySpeedoMeter;
    
    private PrintWriter writer;
    private CarID myCar;
    private GameInitData gameData;
    private List<CarPositionData> carPositions;
    private LapData lapData;
    private List<SpeedoMeter> speedoMeter = new ArrayList<>() ;
    private SpeedoMeter initialSpeedoMeter = new SpeedoMeter();
    private PiecePositionData previousCarPositionData = new PiecePositionData();   
    private int tick = 0;
    private LaneSwitcher laneSwitcher = new LaneSwitcher();
    private int init = 0;
   
    public Main(final BufferedReader reader, final PrintWriter writer, final JoinRace join) throws IOException {
        this.writer = writer;
        String line = null;
        speedoMeter.add(initialSpeedoMeter);
        sendJoinRace(join);
                   
        while((line = reader.readLine()) != null) {
            final MessageFromServer msgFromServer = gson.fromJson(line, MessageFromServer.class);
            switch (msgFromServer.msgType) {
                case "carPositions":                    
                    int carIndex = 0;
                    carPositions = Arrays.asList(gson.fromJson(new Gson().toJson(msgFromServer.data), CarPositionData[].class));
                    for (int i = 0; i < carPositions.size(); i++) {
                        if(carPositions.get(i).id.name.equals("Crazy Taxi")) {
                            carIndex = i;
                        }
                    }
                    
                    if ((laneSwitcher.switchPieceIndexes.size() > 0) 
                            && (carPositions.get(carIndex).piecePosition.pieceIndex == (laneSwitcher.switchPieceIndexes.get(carIndex) - 1))) {
                        if (laneSwitcher.switchLanePreference.get(carIndex) != carPositions.get(carIndex).piecePosition.lane.endLaneIndex) {
                            sendSwitch(new SwitchLanes(laneSwitcher.switchAction(carPositions.get(carIndex))));
                            System.out.println("switched");
                        } else {
                            laneSwitcher.switchLanePreference.remove(0);
                            laneSwitcher.switchPieceIndexes.remove(0);
                            //send(new Throttl(0.6));
                            sendThrottle(new Throttle("throttle", 0.64));
                        }
                    } else {
                        //send(new Throttl(0.6));
                        sendThrottle(new Throttle("throttle", 0.64));
                    }
                                     
                    if(tick > 1) {
                        speedoMeterCalc(carPositions.get(carIndex).piecePosition, previousCarPositionData, gameData.race.track.pieces);
                        //System.out.println(mySpeedoMeter.totalDistance + ", " + mySpeedoMeter.velocity + ", " + mySpeedoMeter.acceleration);
                    } else {
                        SpeedoMeter secondTickSpeedoMeter = new SpeedoMeter();  
                        secondTickSpeedoMeter.updateDistances(carPositions.get(carIndex).piecePosition.pieceIndex, carPositions.get(carIndex).piecePosition.inPieceDistance);
                        secondTickSpeedoMeter.updateSpeed(speedoMeter.get(speedoMeter.size() - 1));
                    }
                    previousCarPositionData = carPositions.get(carIndex).piecePosition;
                    tick++;
                    break;
                case "crash":
                    CarID carCrash = gson.fromJson(new Gson().toJson(msgFromServer.data), CarID.class);
                    System.out.println("Car " + carCrash.name + " crashed");
                    break;
                case "dnf":
                    CarDNF carDNF = gson.fromJson(new Gson().toJson(msgFromServer.data), CarDNF.class);
                    System.out.println("Car " + carDNF.car.name + " DNF");
                    break;
                case "finish":
                    CarID carFinish = gson.fromJson(new Gson().toJson(msgFromServer.data), CarID.class);
                    System.out.println("Car " + carFinish.name + "finsihed");
                case "gameInit":
                    if (init == 0) {
                        System.out.println("Race init");
                        gameData = gson.fromJson(new Gson().toJson(msgFromServer.data), GameInitData.class);
                        System.out.println(gameData.race.track.name);
                        laneSwitcher.calculateIdealSwitches(gameData);
                        for(int i = 0; i < gameData.race.track.pieces.length; i++) {
                            gameData.race.track.pieces[i].updateArcLength(gameData.race.track.lanes);
                        }
                        init = 1;
                    } else {
                        System.out.println(msgFromServer.data);
                    }
                    break;
                case "gameEnd":
                    System.out.println("Race end");
                    break;
                case "gameStart":
                    System.out.println("Race start");
                    break;
                case "join":
                    System.out.println("Joined");
                    break;
                case "lapFinished":
                    lapData = gson.fromJson(new Gson().toJson(msgFromServer.data), LapData.class);
                    System.out.println("Car " + lapData.car.name + " finshed lap " + lapData.lapTime.lap + " in " + lapData.lapTime.millis);
                    laneSwitcher.calculateIdealSwitches(gameData);
                    break;
                case "spawn":
                    CarID carSpawn = gson.fromJson(new Gson().toJson(msgFromServer.data), CarID.class);
                    System.out.println("Car " + carSpawn.name + " spawned");
                    break;
                case "tournamentEnd":
                    System.out.println("Tournament end");
                    break;
                case "yourCar":
                    myCar = gson.fromJson(new Gson().toJson(msgFromServer.data), CarID.class);
                    break;
                default:
                    sendPing(new Ping());
                    System.out.println(msgFromServer.msgType + ": " + msgFromServer.data);
                    break;
            }
        }
    }


    public void speedoMeterCalc(PiecePositionData currectCarPositionData,PiecePositionData prevCarPositionData,Pieces[] trackPieces) {
        mySpeedoMeter = new SpeedoMeter();
        mySpeedoMeter.lane = currectCarPositionData.lane.endLaneIndex;

        if(currectCarPositionData.pieceIndex == prevCarPositionData.pieceIndex) {
            mySpeedoMeter.updateDistances(currectCarPositionData.pieceIndex, currectCarPositionData.inPieceDistance - prevCarPositionData.inPieceDistance);
        } else {
            double distanceTravelledPerTick;
            if(trackPieces[prevCarPositionData.pieceIndex].length != 0) {
                distanceTravelledPerTick = trackPieces[prevCarPositionData.pieceIndex].length - prevCarPositionData.inPieceDistance;
            } else {
                distanceTravelledPerTick = Math.abs(trackPieces[prevCarPositionData.pieceIndex].arclength[prevCarPositionData.lane.endLaneIndex]) - prevCarPositionData.inPieceDistance;
            }
            distanceTravelledPerTick = distanceTravelledPerTick + currectCarPositionData.inPieceDistance;
            mySpeedoMeter.updateDistances(currectCarPositionData.pieceIndex,distanceTravelledPerTick);
        }
        mySpeedoMeter.updateSpeed(speedoMeter.get(speedoMeter.size()-1));
        speedoMeter.add(mySpeedoMeter);
    }
    
    private void sendJoinRace(JoinRace joinRace) {
        writer.println(new Gson().toJson(joinRace));
        writer.flush();
    }
    
    private void sendThrottle(Throttle throttle) {
        writer.println(new Gson().toJson(throttle));
        writer.flush();
    }
    
    private void sendPing(Ping ping) {
        writer.println(new Gson().toJson(ping));
        writer.flush();
    }
    
    private void sendSwitch(SwitchLanes switchCommand) {
        writer.println(new Gson().toJson(switchCommand));
        writer.flush();
    }
}